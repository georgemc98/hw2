## Homework 2

**In this homework, we're going to explore files and filesystems**

- Just like last time, start by forking this repo into your own Gitlab.com account (via web), and then cloning that repo to your local machine (e.g.: `git clone git@gitlab.com:<your-username>/hw2.git`).

- I will ask questions as we go, and I expect you to answer them inside this file by using a text editor.
For example:

```markdown
#### [x pts] Fake Section:                      	(my text)
> Questions:               				    		(my text)
    - What kind of file are you reading?    		(my text)
        - It is a markdown file             		(your answer -- added with a text editor)
```

- I will also ask you to take a lot of screenshots.  I don't want/need your whole screen. Find a way to grab a small rectangular portion of your screen, so you can just capture what you need.  
	+ when I ask for screenshots, I will write something like:

	```markdown
	- Here is an instruction step
		+ screencap (capfile.png)
	```
	
	that means, take a screencap of the last step, and save it here in this repo as `capfile.png`.  If you want to get fancy, you can make a directory here called `screenshots` or something and keep all your captures inside.

- also, keep in mind that if one of the arguments in the command I give is a file, then you should be working in a directory that contains that file.  For example, when I run `cat mynewfile`, the `cat` program has to be able to find `mynewfile`. `cat` will not search your system for you.  You have to tell it exactly where that file is.  If I first`cd` to the directory which contains `mynewfile`, then I can just use the command as written.  Otherwise, I have to use `cat /full/path/to/mynewfile` so that `cat` can find it. So, if a command is operating on a file, you should check that either: A) the file is inside your current `$PWD` or B) you give the full path to the file, so the program you're calling can find it.

- several commands may require elevated permissions. If you get an error message something like "bad permissions" or "not readable," try using `sudo` in front of the same command

#### [3 pts] Inspect Megi's Multiboot Image for the Pinephone:

- read the instructions at https://xnux.eu/p-boot-demo/ 

- the files are hosted by Megi at http://dl.xnux.eu/p-boot-multi-2020-11-23/  and someone else is hosting a mirror at http://mirror.uxes.cz/dl.xnux.eu/p-boot-multi-2020-11-23/  (I've found the mirror to be faster).  I will be "seeding" the file via torrent, so maybe you can get it from me :)

- each of those directories contain three files:
  
  1. SHA256
  
  2. SHA256.asc
  
  3. multi.img.gz

- download all the files (you may use the .torrent link to get `multi.img.gz`)

- move your downloaded files into this `hw2` directory

> Questions:
>- What kind of file did you just download?
	-The files included are one .ASC file, which provides an encryption key to verify that the image included in the download are authentic.  The .IMG.GZ file is an image that will need to be decompressed before use.
>- Why do we call it an "image"?
	-The Image file contains an exact rendering of how the PinePhone's hardware and functionality.  It can be distributed a number of ways, but no matter the distribution method, the image stays the same.
>- Why would Megi use this format to distribute it?
	-Megi chose this format because torrenting allows for decentralized hosting that tends to make downloads faster, granted that there are numerous hosts.  The size of the file makes it expensive for a single host to distribute the files to downloaders.

#### [3 pts] .gitignore:
**DO NOT ADD THESE NEW FILES TO THE REPOSITORY -- THEY ARE WAY TOO LARGE (and I already have them)**
  
- you can tell `git` to ignore certain files by making a list of things to ignore inside a new file called `.gitignore`  (remember that the `.` prefix means "hidden"?)
  
- edit the contents of the file with `nano .gitignore`. Make sure it looks like:
    
    ```
    (there may be other stuff here)
    SHA256
    SHA256.asc
    multi.img.gz
    *.img
    *.img.*
    ```
    
    
#### [3 pts] Hashes:

- use `cat SHA256` to see the contents of `SHA256`
    
	- save a screencap as "catsha.png"

- run the command `sha256sum multi.img.gz`
  
	- save a screencap as "shasum.png"

> Questions:
>   
>   - How do the output of the last two commands compare?
	-"cat SHA256" provides the checksum for both "multi.img" and "multi.img.gz." "sha256sum multi.img.gz" shows the checksum for only "multi.img.gz."
>   - Why would MEGI distribute the `SHA256` file with the image? 
	-SHA256 is an encryption method included to verify that the image is indeed authentic.  This prevents fake image distributions. 
> 


#### [3 pts] Look at the Multiboot image (download file):

- use the command `head` to show you the beginning of the file (contents)
	- screencap (headimggz.png)

> Questions:
> 	- Why does it look funny?  (use `head` to look at this .md file for comparison)
	-The head command cannot display the data correctly because the .gz file has not been decompressed.


#### [6 pts] Extract the actual image from what you downloaded

- use `zcat multi.img.gz > multi.img` to decompress the image file
 

- take a look at the head of the file with `head multi.img`
	+ screencap (headimg.png)

- take a look at the head in a hexdump format:
	+ `head multi.img | xxd`
	+ screencap (headimgxxd.png)
	
> ##### Questions:
> - what does "`zcat`" mean (look it up)?
	-"zcat" allows users to see the contents of a file without decompressing it
> - what happens after running this command?
	-zcat decompresses the file into multi.img	
> - what do the contents of the new file look like as compared to the *.gz version?
	-When "head multi.img" is run, the file looks the same.  When " head multi.img | xxd" is run, the hexdump representation of the image is shown	
#### [6 pts] Deeper inspection of image file

- run `sha256sum multi.img` on the new file
	+ screencap (shasumimg.png)

- use `file multi.img` to see if we can learn anything about the file
	+ screencap (filemulti.png)

- use `fdisk -l multi.img` to get more info about the file system on the image
	+ screencap (dumpemulti.png)

>Questions:
> - what do we know about the decompressed file `multi.img` ?
	-We know the start of the partition 1 is at 0x0 and ends at 0x19.  Partition 2 starts at 0x19 and ends at 0xfa.
> - what does it contain?
	-It contains two different partitions that take up 20070040 sectors in my hard drive
> - how did the output of the `sha256sum` command compare to the info in the `SHA256` file?
	-The "sha256sum" command only showed the checksum of the image file.  The information of the SHA256 shows far more information, including total partitions' sizes, and where they start in memory
#### [6 pts] Make the image look like a block-device to the system

- run `losetup -f multi.img` to make the file look like a block-device (hard disk) to the system ( you may have to run this as a superuser by prefixing the command with`sudo`)

- run `losetup -a` to get a list of all "loopback" devices (what we just made)

- run `losetup -d /dev/loop##` (##==our number) to remove ("delete") our loopback device

- run `losetup -a` to confirm it's gone

- make it again :)

- `losetup -a` to see what our loop device number is (/dev/loop##) in case it changed

- find our file in the list
	+ screencap a couple lines of output including the one with our file (losetup.png)

- run `lsblk -f` to "list block-devices" with "full" output

- try `fdisk -l /dev/loop##` again on our new loopback device (replace ## with the correct number from above)

- try `file -s` again on the loopback device:
	+ `sudo file -s /dev/loop##`   (replace ## with the correct number from above)
	
- run `losetup -d /dev/loop##` (##==our number) to remove ("delete") our loopback device (again)

#### [6 pts] Recreate loopback device and scan

We are now going to re-create our loopback device again(!) but this time, we might add the argument `-P` to the `losetup` command.  Let's check the `losetup` manual to see what this will do:

+ `man losetup`  (the `man` command automatically uses `pager` which by default is `less` to view the manual.  use the arrows and `b` and `<space>` to move around and `<q>` to quit.)
	
	+ screencap portion of manual explaining `-P` parameter (manlosetup.png)
	
- Go ahead and do it:
	+ `losetup -fP multi.img`
	+ `losetup -a`

- run `lsblk -f` again, and screencap the portion of output with our newest loopback device


>Questions:
>	- did we get a different response from `file` or `fdisk` when using the virtual (loopback) block device?
	-When running the "file" command, I got the same output as when I ran "file multi.img." When I ran fdisk, I got the same output as when I ran "fdisk multi.img"
>	- is there anything suspicious about the output of either of those commands?
	-Its the exact same as when we ran the same commands on the decompressed image
>	- What happened after we ran `losetup -P ...`??  Why??
	-Not really sure what step this question is referring to.  We ran "losetup -fP multi.img" and this makes the file look like a hard drive and forces the kernel to perform a scan on the partition table.

#### [6 pts] Filesystem info (again)

Now that the block device (and any partitions) exist as virtual devices, let's look at the partitions individually:

- run `lsblk -af` to list all block devices.  Pay attention to our loopback device and make note of its partitions

- let's run `file -s /dev/loop##p#>` and `fdisk -l /dev/loop##p#` again on each partition to see if anything's changed.
	+ screencaps (filespart1.png, filespart2.png, fdiskpart1.png, fdiskpart2.png)


#### [10 pts] Mount the loopback devices

The main directory tree in Linux (the one we navigate with `cd` and `ls`) is a virtual filesystem.  It's the heart --and entirety-- of the whole system.  Any _real_ filesystems found on external drives, etc. have to be "mounted" into the main (virtual) system directory tree before they can be accessed or navigated.

- create a place to mount our devices:
	+ `sudo mkdir /mnt/pb1`
	+ `sudo mkdir /mnt/pb2`

- mount our loopback devices there:
	+ `sudo mount /dev/loop##p1 /mnt/pb1`
	+ `sudo mount /dev/loop##p2 /mnt/pb2`

- list the contents of the partitions:
	+ `ls /mnt/pb1`
	+ `ls /mnt/pb2`
	+ screencap (lspb1.png, lspb2.png)
	
>Questions:
>	- did these commands both succeed?
	-No. We could not mount loop0p1 on pb1 
>	- If not, which one failed and why?
	-loop0p1's mount failed
>	- what is the reason, do you think?
	-I believe it failed because partition 2 is the one that connects to the rest of the device, while partition 1 is the untouchable "kernel."
#### [3 pts] Unmount the loopback devices

After adding external filesystems into the main (virtual) system FS, we might want to remove it.

- See a list of all mounted filesystems with `mount`
- Unmount the loopback devices from the system fs with `sudo umount /dev/loop##p#` (yes, it's "umount" and not "unmount")
>Questions:
	- where does the _real_ file system on the loopback device reside?
	-The real file system on the loopback devices resides now in the pb2 folder	
	- what happens to it after we unmount the device?
	-The files in the pb2 folder disappear	
